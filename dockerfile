FROM node:14
WORKDIR /usr/app
COPY package.json .
COPY . .
RUN npm install
CMD npm run start
